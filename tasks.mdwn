[[!meta title="Tasks"]]

# Tasks

These are some of the coding pieces we are actively working on.  If
you want to help with any these, please say so [[on the list|discuss]] or contact
James!

 * Patch FreedomMaker to bring up eth0 automatically so the box can be accessed without a JTAG cable.

 * Replacing ssl hierarchy with a gpg-based web of trust.  This involved
some scripting, some work with ssl certs, modfiying mod-gnutls and maybe
some browser plugins.

 * Improving the FreedomBox Privoxy filters.  We need test scripts and
more/better filters.  Reimplementing the current hack that lets privoxy
do multiple redirect URLs as a proper bit of C code would also be quite
useful.

 * Proxying.  This one is easy: we could use a script that sets up a
port-forwarded connection to the freedombox-privoxy server for a
particular user and secures that connection via monkeysphere.

 * Config layer design.  We would like the front end to talk to a middle
layer that handles system config.  That middle layer then talks to the
system and sets options (e.g. by twiddling text files).  No work had
been done on that middle layer so far and it needs to be designed from
the ground up.  [[Augeas|http://augeas.net/]] might be the place to start on this.

 * Front-end work.  Plinth is our current front end proposal.  It could use
design work, cleanup and reimagining.

 * User database.  A freedombox serves/interacts with users.  We need ACLs
for them, settable via the front-end.  In the current vision, users are
identified primarily via their gpg key.

 * Welcome script. When a user first plugs in a virgin freedombox, she
should be walked through setup, key generation, etc.  We need a web
script that does this interaction in an informative, no-hassle way and
computes/generates any other needed data.

 * Santiago Port.  We have started experimenting with using a tor
hidden service as a way to find FreedomBoxes wherever they might be.
A FreedomBox should listen on its hidden service port and respond to
login requests and informational queries (e.g. "How can I reach you,
what is your ip?").  Tor is too slow to pass all our traffic on it as
a matter of course, so communication over this Santiago port is really
about setting up further communication over other channels.  James
started to extend Plinth to do this, but hasn't published the branch
yet.  If anybody is interested in helping with this, we'll put the
branch up.

 * Many people believe we FreedomBoxes should be dropboxes.  So far,
   nobody has started this project.  Secure, private dropbox would be
   quite useful.
