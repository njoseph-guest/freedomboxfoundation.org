# Project news from the FreedomBox Foundation. 

This page contains an archive of all past news items

The best way to keep up with project news is by subscribing to our [RSS](http://www.freedomboxfoundation.org/index.rss) or [Atom](http://www.freedomboxfoundation.org/index.atom) feed. 

Contact information for press and any other inquires is located on the [[Contact]] page.

## News Archive

[[!inline pages="page(news/*) and currentlang()" rootpage="news/" postform=yes actions=yes show=0]]
