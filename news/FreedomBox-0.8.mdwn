## FreedomBox 0.8 Released

*January 20th, 2016*

I'm pleased to announce that FreedomBox 0.8 has been released! This release
comes 9 weeks after the previous release (0.7).

We did not build new images for this release. However it is available in Debian
(unstable) as 2 packages, freedombox-setup 0.8 and plinth 0.8.1-1.

If you are using Freedombox 0.7, you can upgrade to 0.8. (If you have automatic
upgrades enabled, this should have happened already!) But first, please read
the "Known Issues" section below.

 
More information on this release is available on the wiki:

<https://wiki.debian.org/FreedomBox/ReleaseNotes>

Major FreedomBox 0.8 Changes:

- Added Quassel, an IRC client that stays connected to IRC networks and can
  synchronize multiple frontends. 
- Improved first boot user interface. 
- Fixed Transmission RPC whitelist issue. 
- Added translations for Turkish, Chinese, and Russian. Fixed and updated
  translations in other languages. 
- Added Monkeysphere, which uses PGP web of trust for SSH host key verification. 
- Added Let's Encrypt, to obtain certificates for domains, so that browser
  certificate warnings can be avoided. 
- Added repro, a SIP server for audio and video calls. 
- Allow users to set their SSH public keys, so they can login over SSH without
  a password.

Known Issues:

There is an issue in Plinth 0.7.x that can affect those trying to upgrade to
0.8.1. The issue happens when the manual upgrade is started (by clicking the
"Upgrade now" button) and it tries to upgrade Plinth. The Plinth upgrade can
fail during this manual upgrade process started through Plinth.

There are 2 workarounds for those trying to upgrade from 0.7.x:
- Turn on Automatic Upgrades, and wait (up to 24 hours). Plinth will be
  automatically upgraded to 0.8.1, and the issue is avoided.
- Or, you can SSH into the box, and run "sudo apt update && sudo apt upgrade".


Thanks to all who helped to put this release together. There were several new
contributors for this release. The wiki has a full list of contributors to the
project:

<https://wiki.debian.org/FreedomBox/Contributors>

Please feel free to join us to discuss this release on the mailing list, IRC,
or on the monthly progress calls:

- List: <http://lists.alioth.debian.org/pipermail/freedombox-discuss/>

- IRC: <irc://irc.debian.org/freedombox>

- Calls: <https://wiki.debian.org/FreedomBox/ProgressCalls>
