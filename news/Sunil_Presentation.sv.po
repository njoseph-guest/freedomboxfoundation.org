# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-10-19 16:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "##FreedomBox 0.6 Presentation \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*October 19, 2015*\n"
msgstr ""

#. type: Plain text
msgid ""
"Sunil is doing a presentation at the Software Freedom Law Center's annual "
"conference on October 30th. He will be showing off the forth coming 0.6 "
"release. (Look for the 0.6 release any day now)"
msgstr ""

#. type: Plain text
msgid ""
"The Conference is at the Columbia Law School on Friday October 30th. Sunil's "
"presentation will start at around 2:45 pm. The Conference is free. You can "
"find out more information about it at the [Software Freedom Law Center's "
"website](http://softwarefreedom.org/events/2015/sflc-fall-conference/)"
msgstr ""
