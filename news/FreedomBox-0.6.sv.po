# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-11-02 20:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "##Freedombox 0.6 Released!\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "*October 31, 2015*\n"
msgstr ""

#. type: Plain text
msgid "Hello,"
msgstr ""

#. type: Plain text
msgid ""
"I'm pleased to announce that FreedomBox version 0.6 has been released! This "
"release comes 2 months after the previous, 0.5 release."
msgstr ""

#. type: Plain text
msgid ""
"Please feel free to join us to discuss this release on the mailing list, "
"IRC, or on the monthly progress calls:"
msgstr ""

#. type: Plain text
msgid "- List: http://lists.alioth.debian.org/pipermail/freedombox-discuss/"
msgstr ""

#. type: Plain text
msgid "- IRC: irc://irc.debian.org/freedombox"
msgstr ""

#. type: Plain text
msgid "- Calls: https://wiki.debian.org/FreedomBox/ProgressCalls"
msgstr ""

#. type: Plain text
msgid "The FreedomBox version 0.6 is available here:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    http://ftp.skolelinux.org/pub/freedombox/0.6/\n"
msgstr ""

#. type: Plain text
msgid ""
"Before using, you should verify the image's signature, see "
"https://wiki.debian.org/FreedomBox/Download for further instructions:"
msgstr ""

#. type: Plain text
msgid ""
"$ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys "
"0x36C361440C9BC971"
msgstr ""

#. type: Plain text
msgid "$ gpg --fingerprint 0x36C361440C9BC971"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"    pub   4096R/0C9BC971 2011-11-12\n"
"          Key fingerprint = BCBE BD57 A11F 70B2 3782\n"
"                            BC57 36C3 6144 0C9B C971\n"
"    uid                  Sunil Mohan Adapa <sunil@medhas.org>\n"
"    sub   4096R/4C1D4B57 2011-11-12\n"
msgstr ""

#. type: Plain text
msgid ""
"$ gpg --verify "
"freedombox-unstable_2015-10-18_raspberry-armel-card.tar.bz2.sig "
"freedombox-unstable_2015-10-18_raspberry-armel-card.tar.bz2"
msgstr ""

#. type: Plain text
msgid "(Replace the file names with the version you download.)"
msgstr ""

#. type: Plain text
msgid "Thanks to all who helped put this release together."
msgstr ""

#. type: Plain text
msgid "More information on this release is available on the wiki:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "    https://wiki.debian.org/FreedomBox/ReleaseNotes\n"
msgstr ""

#. type: Plain text
msgid "Major FreedomBox 0.6 Changes:"
msgstr ""

#. type: Bullet: '- '
msgid "New supported hardware target: Raspberry Pi 2"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"- New modules in Plinth:\n"
"  * Shaarli: Web application to manage and share bookmarks.\n"
"  * Date & Time: Configure time zone and NTP service.\n"
"  * Service Discovery: Configure Avahi service.\n"
"  * Debian packages can be download over Tor.\n"
"- Switched from mod_ssl to mod_gnutls.\n"
"- Documentation Revamp:\n"
"  * Get latest FreedomBox user manual from wiki.\n"
"  * Manual is focused on using FreedomBox, with a section on developing\n"
"    Plinth modules.\n"
"- Wi-Fi configuration now in general network configuration.\n"
msgstr ""

#. type: Plain text
msgid "Known Bugs:"
msgstr ""

#. type: Bullet: '- '
msgid ""
"DreamPlugs do not boot the 0.6 image.  Please upgrade your previous "
"Dreamplug by manually upgrading Plinth to 0.6."
msgstr ""

#. type: Plain text
msgid "Nick"
msgstr ""
