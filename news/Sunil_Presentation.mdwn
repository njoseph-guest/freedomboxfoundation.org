##FreedomBox 0.6 Presentation 

*October 19, 2015*

Sunil is doing a presentation at the Software Freedom Law Center's annual conference on October 30th. He will be showing off the forth coming 0.6 release. (Look for the 0.6 release any day now)

The Conference is at the Columbia Law School on Friday October 30th. Sunil's presentation will start at around 2:45 pm. The Conference is free. You can find out more information about it at the [Software Freedom Law Center's website](http://softwarefreedom.org/events/2015/sflc-fall-conference/)
