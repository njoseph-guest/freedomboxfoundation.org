[[!meta title="Introducing the TAC"]]

# Introducing the Technical Advisory Committee

As the community continues to push the FreedomBox from idea towards
reality, it is time to expand our technical leadership team.  We are
happy to announce the formation of a technical advisory committee to
help coordinate and guide the development efforts of this project.
This advisory committee is already underway, with an initial
membership of industry leaders including:

* Bdale Garbee, Open Source & Linux Chief Technologist at
  Hewlett-Packard,

* Jacob Appelbaum from the Tor project,

* Sam Hartman, former Chief Technologist at the MIT Kerberos Consortium
  and IETF Security Area Director,

* Sascha Meinrath, Director of the New America Foundation's Open
  Technology Initiative,

* Rob Savoye, long-time GNU hacker, Gnash lead developer, and winner
  of the 2010 award for the Advancement of Free Software

* Matt Zimmerman, former Canonical CTO

We'll be hearing more from the TAC over the coming weeks and
months. Anyone interested in following the activity of the advisory
committee as it happens is welcome to check out the public archives of
their email list at [http://lists.freedomboxfoundation.org/s/arc/tac](http://lists.freedomboxfoundation.org/s/arc/tac) (the list is for TAC members, so please do not attempt to subscribe).
If you want to talk to the TAC in real time, they can be found in #freedombox-tac on irc.oftc.net.

[[!sidebar  content="""

Links:    
[[Home|https://www.freedomboxfoundation.org/]]    
[[FAQ]]    
[[Donate]]    

"""]]
