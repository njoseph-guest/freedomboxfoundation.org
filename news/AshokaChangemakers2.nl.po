# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-11-29 15:12+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Dutch (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/nl/)\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"FreedomBox Wins Ashoka Changemakers Competition\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "FreedomBox Wins Ashoka Changemakers Competition"
msgstr ""

#. type: Plain text
msgid ""
"The FreedomBox Foundation has [won the Ashoka Changemakers Competition]"
"(<http://www.changemakers.com/citizenmedia>)  in the \"Citizen Media\" "
"Category! This event was decided by a public vote, which means it was *your "
"help* that pushed us over the top.  Thank you to everybody who voted and "
"helped spread the word.  This community continues to work together in "
"amazing ways."
msgstr ""

#. type: Plain text
msgid ""
"Ashoka will award us $5,000, which we will use to fund further development "
"of the FreedomBox.  One of our goals is raising awareness of the need for "
"privacy-respecting technology, and participating in the competition allowed "
"us to present the FreedomBox to a lot of people who had never heard of it "
"before.  On that basis alone, this competition was worthwhile for the "
"FreedomBox."
msgstr ""

#. type: Plain text
msgid ""
"Congratulations to all the other winners and finalists.  Ashoka spotlighted "
"many good projects working toward freedom and open access to communications "
"technology.  FreedomBox will surely cross paths with those projects again."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""
