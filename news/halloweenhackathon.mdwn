##FreedomBox Halloween Hack-a-thon 
*Saturday October 31st, 2015*

The FreedomBox Foundation is hosting a Halloween hack-a-thon on
Saturday October 31st at the offices of the [Software Freedom Law Center](http://www.softwarefreedom.org/)
from 10 am to 6 pm, near Lincoln Center in New York City. New and
existing contributors will be getting together to work on our
Debian-based privacy-respecting self-hosting software suite and wireless
router.

Members of the FreedomBox core development team will be on hand to help
new contributors get involved and to get some actual work done! We are
going to be talking about:

* how to use a FreedomBox as a secure home router;
* using existing Debian packages to integrate them into FreedomBoxes;
* integrating in a secure decentralized social network;
* replacing passwords with an integrated authentication system based on PGP;

We live in a world where our use of the network is mediated by
organizations that often do not have our best interests at heart. To
regain control of our privacy, the [FreedomBox project](https://wiki.debian.org/FreedomBox/) is building an integrated
suite of software that does not rely on centralized services. Of course
much of this software already exists. We are hacking, tweaking, and
molding it into a easy to use software suite so everyone can regain
control of their privacy by running a FreedomBox at home.

Bring yourselves, your ideas, your PGP keys, and your Raspberry Pis (or
other device you want to try FreedomBox on!) We will bring the pizza,
over-caffeinated soda, and Halloween candy.


Time: 10:00 am to 6:00 pm, October 31st, 2015
Location: Software Freedom Law Center,
 1995 Broadway, 17th Floor, New York, NY

Email us at hackathon at freedomboxfoundation.org to let us know you are coming so we can be sure to order enough food and
caffeine email.

