# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# markfiers42 <mark.fiers.42@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2012-05-30 23:37+0000\n"
"Last-Translator: markfiers42 <mark.fiers.42@gmail.com>\n"
"Language-Team: Dutch (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/nl/)\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "#Eben Moglen video at Internet Evolution \n"
msgstr "#Eben Moglen video op \"Internet Evolution\"\n"

#. type: Plain text
msgid ""
"While at Personal Democracy Forum last week, Eben was interviewed by Nicole "
"Ferraro of Internet Evolution. The first part of that video, focusing on "
"defining what the FreedomBox is, has now been put online here: [Internet "
"Evolution](http://www.internetevolution.com/video.asp?"
"section_id=1361&doc_id=207273)."
msgstr ""
"Vorige week, op het \"Personal Democracy Forum\", is Eben geïnterviewd door "
"Nicole Ferraro   van \"Internet Evolution\". Het eerste deel van de video "
"gaat over de definitie van wat de FreedomBox is, en is online hier te "
"vinden: [Internet Evolution](http://www.internetevolution.com/video.asp?"
"section_id=1361&doc_id=207273)."

#. type: Plain text
msgid "Further videos still to come next week."
msgstr "Meer video's komen volgende week."
