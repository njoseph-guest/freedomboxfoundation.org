# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-05-16 21:21+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Telugu (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/te/)\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Title #
#, no-wrap
msgid "Open Source Veteran Bdale Garbee Joins FreedomBox Foundation Board"
msgstr ""

#. type: Plain text
msgid ""
"NEW YORK, March 10, 2011-- The [FreedomBox Foundation](http://"
"freedomboxfoundation.org), based here, today announced that Bdale Garbee has "
"agreed to join the Foundation's board of directors and chair its technical "
"advisory committee. In that role, he will coordinate development of the "
"FreedomBox and its software."
msgstr ""

#. type: Plain text
msgid ""
"Garbee is a longtime leader and developer in the free software community. He "
"serves as Chief Technologist for Open Source and Linux at Hewlett Packard, "
"is chairman of the Debian Technical Committee, and is President of Software "
"in the Public Interest, the non-profit organization that provides fiscal "
"sponsorship for the Debian GNU/Linux distribution and other projects. In "
"2002, he served as Debian Project Leader."
msgstr ""

#. type: Plain text
msgid ""
"\"Bdale has excelled as a developer and leader in the free software "
"community. He is exactly the right person to guide the technical "
"architecture of the FreedomBox,\" said Eben Moglen, director of the "
"FreedomBox Foundation."
msgstr ""

#. type: Plain text
msgid ""
"\"I'm excited to work on this project with such an enthusiastic community,\" "
"said Garbee.  \"In the long-term, this may prove to be most important thing "
"I'm doing right now.\""
msgstr ""

#. type: Plain text
msgid ""
"The Foundation's formation was announced in Brussels on February 4, and it "
"is actively seeking funds; it recently raised more than $80,000 in less than "
"fifteen days on [Kickstarter](http://www.kickstarter.com/projects/721744279/"
"push-the-freedombox-foundation-from-0-to-60-in-30/)."
msgstr ""

#. type: Title ##
#, no-wrap
msgid "About the FreedomBox Foundation"
msgstr ""

#. type: Plain text
msgid ""
"The FreedomBox project is a free software effort that will distribute "
"computers that allow users to seize control of their privacy, anonymity and "
"security in the face of government censorship, commercial tracking, and "
"intrusive internet service providers."
msgstr ""

#. type: Plain text
msgid ""
"Eben Moglen is Professor of Law at Columbia University Law School and the "
"Founding Director of the FreedomBox Foundation, a new non-profit "
"incorporated in Delaware.  It is in the process of applying for 501(c)(3) "
"status.  Its mission is to support the creation and worldwide distribution "
"of FreedomBoxes."
msgstr ""

#. type: Plain text
msgid ""
"For further information, contact Ian Sullivan at press@freedomboxfoundation."
"org or see <http://freedomboxfoundation.org>."
msgstr ""
