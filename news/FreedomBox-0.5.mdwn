##Freedombox 0.5 Released!

*August 7, 2015*

We are pleased to announce that FreedomBox version 0.5 has been released!
This release comes 7 months after the previous, 0.3 release.

The FreedomBox version 0.5 is available here:

	http://ftp.skolelinux.org/pub/freedombox/0.5/

Before using, you should verify the image's signature, see
https://wiki.debian.org/FreedomBox/Download for further instructions:

$ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys
0x36C361440C9BC971

$ gpg --fingerprint 0x36C361440C9BC971

    pub   4096R/0C9BC971 2011-11-12
          Key fingerprint = BCBE BD57 A11F 70B2 3782
                            BC57 36C3 6144 0C9B C971
    uid                  Sunil Mohan Adapa <sunil@medhas.org>
    sub   4096R/4C1D4B57 2011-11-12

$ gpg --verify
freedombox-unstable_2015-08-06_raspberry-armel-card.tar.bz2.sig
freedombox-unstable_2015-08-06_raspberry-armel-card.tar.bz2

(Replace the file names with the version you download.)

Thanks to all who helped put this release together.

More information on this release is available on the wiki:

	https://wiki.debian.org/FreedomBox/ReleaseNotes

Major FreedomBox 0.5 Changes:

- New targets: CubieTruck, i386, amd64

- New apps in Plinth: Transmission, Dynamic DNS, Mumble, ikiwiki,
Deluge, Roundcube, Privoxy

- NetworkManager handles network configuration and can be manipulated
through Plinth.

- Software Upgrades (unattended-upgrades) module can upgrade the system,
and enable automatic upgrades.

- Plinth is now capable of installing ejabberd, jwchat, and privoxy, so
they are not included in image but can be installed when needed.

- User authentication through LDAP for SSH, XMPP (ejabberd), and ikiwiki.

- Unit test suite is automatically run on Plinth upstream. This helps us
catch at least some code errors before they are discovered by users!

- New, simpler look for Plinth.

- Performance improvements for Plinth.

Please feel free to join us to discuss this release on the mailing list,
IRC, or on the monthly progress calls:

- List: http://lists.alioth.debian.org/pipermail/freedombox-discuss/

- IRC: irc://irc.debian.org/freedombox

- Calls: https://wiki.debian.org/FreedomBox/ProgressCalls
