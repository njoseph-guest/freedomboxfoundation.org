# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-11-07 01:02+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Spanish (Argentina) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/es_AR/)\n"
"Language: es_AR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"What Is A Distributed Social Network?\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "What Is A Distributed Social Network?"
msgstr ""

#. type: Plain text
msgid ""
"J David Eisenberg made an excellent comic [introduction to distributed "
"social networks](http://dsn-test.com/dsn-vn/).  For anybody who isn't quite "
"sure why the FreedomBox is important, that's a fun and non-technical way to "
"explain it."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""
