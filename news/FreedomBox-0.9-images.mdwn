## FreedomBox 0.9 Images Available

*July 15th, 2016*

A week ago we have completed building FreedomBox images for 0.9 as
0.9-rc2 and made them available for release testing.  No major issues
have been found in the images and they are now the 0.9 final release
images.

   * <http://ftp.skolelinux.org/pub/freedombox/0.9/>
   * <http://ftp.freedombox.org/pub/freedombox/0.9/>

### Transition to Debian testing:

With this release, we have completed the transition to Debian "testing"
from Debian "unstable".  From now on, for regular users, images will be
based on "testing".  When using these images, upgrades will happen to
only packages that end up in Debian "testing".

If you are already using FreedomBox, you are advised to switch to
"testing" distribution manually for higher stability.  This can done by
replacing "unstable" with "testing" in /etc/apt/sources.list and waiting
for a few weeks for all packages to settle down or by freshly setting up
using "testing" images.

Images based on Debian "unstable" will still be built and available as
"nightly" images so that contributors can test early and prevent the
regular users from facing issues.  If you wish to contribute to
FreedomBox by finding and reporting early problems, please stick with
"unstable" images.
