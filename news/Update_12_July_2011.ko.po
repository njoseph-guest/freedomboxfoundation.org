# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2011-07-13 00:50+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Korean (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/ko/)\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"FreedomBox Update\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "FreedomBox Update"
msgstr ""

#. type: Plain text
msgid ""
"A lot of people have been asking for an update, which is a good indication "
"that we need to update folks more often."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<a href=\"http://www.gag.com/~bdale/\">Bdale Garbee</a> will be at <a\n"
"href=\"http://debconf11.debconf.org/\">DebConf11</a>.  He and\n"
"[Clint Adams](http://people.debian.org/~clint/) will be running a\n"
"hackfest.  If you are going to be there (a lot of FreedomBoxers are\n"
"also <a href=\"http://wiki.debian.org/FreedomBox\">Debian devs</a>),\n"
"please track him down and ask him any question you have about the box.\n"
"His answers will be thoughtful and perhaps surprising.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"I will be at <a href=\"http://contactcon.com/\">ContactCon</a>. If you\n"
"are going, please <a\n"
"href=\"http://freedomboxfoundation.org/contact/\">let me know</a> so we\n"
"can connect!\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"I'm told the smart phone high five has been the subject of some\n"
"fevered hacking.  <a href=\"http://maffulli.net/\">Stefano Maffulli</a>\n"
"organized hacking on that in small community events and it is starting\n"
"to produce useful work.  Now to get that code in to a gittable place!\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The need for a roadmap is clear.  Everywhere I go people want a\n"
"framework into which they can start putting their work.  The <a\n"
"href=\"http://freedomboxfoundation.org/news/IntroducingTheTAC/index.en.html\">TAC</a>\n"
"is pondering that and I hope we will have it shortly.  In the short\n"
"term, Bdale and the TAC are working on a build release.  This will be\n"
"a basic build system on top of which we can start putting packages.\n"
"It will give us all a common reference point to hack from.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"The <a\n"
"href=\"http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss\">mailing\n"
"list</a> is a hotbed of development discussion.  <a\n"
"href=\"http://dr.jones.dk/\">Jonas Smedegaard</a> is a one-man packaging\n"
"machine (I hear he'll be at DebConf too!), and that's a huge step.\n"
"Others projects, like <a href=\"https://pagekite.net/\">PageKite</a> are\n"
"building pieces that we hope can be integrated into FreedomBox soon.\n"
"Debian-style development is chaotic.  There are too many ideas to take\n"
"them all, but before this is through, I think we'll have considered\n"
"every possible combination of software.  I hope that shortly some of\n"
"those discussions will result in meta-packages that configure\n"
"combinations of software to work nicely together.\n"
msgstr ""

#. type: Plain text
msgid ""
"Speaking of configuration, we are thinking hard about a configuration "
"process and model.  With the many possible package configurations, each with "
"its own method of storing configuration and state, handling conflicts (as "
"well as expert-user tweaks made outside the normal interface) will be "
"difficult.  We have some design ideas for that structure, but I sense this "
"is an area where we will adopt somebody else's design rather than invent "
"anything new."
msgstr ""

#. type: Plain text
msgid ""
"Administratively, we've assembled the Foundation pieces.  We have a board.  "
"Yesterday, the board converted me from a presumptuous volunteer into the "
"executive director.  I don't think that changes what I will do except it "
"allows me to do more of it and I can feel a little more comfortable making "
"statements about what the Foundation is up to."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"We have done a *lot* of work with <a\n"
"href=\"http://www.marvell.com/\">Marvell</a> and <a\n"
"href=\"http://www.globalscaletechnologies.com/\">Global Scale</a>.\n"
"Helping the box manufacturers streamline license compliance is a big\n"
"task, but we've been making real progress.  Clint identified some\n"
"parts of the <a\n"
"href=\"http://www.globalscaletechnologies.com/c-5-dreamplugs.aspx\">Dream\n"
"Plug</a> that didn't build properly or for which the true source\n"
"wasn't available.  After some dialog with upstream, we're getting all\n"
"that source.  The next step is helping upstream publish that code in a\n"
"routine way.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Stefano and I have been searching for UX and human interface designers\n"
"who might help us with one of the most difficult parts of this\n"
"project.  <a href=\"http://emoglen.law.columbia.edu/\">Eben Moglen</a>,\n"
"Ian Sullivan, Bdale and I have had many phone conversations about how\n"
"the user configures the box.  We agree it needs as few buttons as\n"
"possible.  We agree it needs sane defaults as well as expert modes.\n"
"We agree it listens on port 80 but also talks to your phone.  Beyond\n"
"that, we agree we need expert help.\n"
msgstr ""

#. type: Plain text
msgid ""
"We have had offers of help from hacker spaces in California and Texas! We "
"would like to connect with as many hacker spaces as possible.  Stefano and I "
"are trying to make a hackfest-in-a-box kit and hack spaces are the perfect "
"place to deploy those kits.  If you are involved in a hack space and can "
"pull some awesome geeks together for a night of fun, I want to talk to you."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Finally, I have ordered stickers and t-shirts so we will finally get\n"
"those to our <a\n"
"href=\"http://www.kickstarter.com/projects/721744279/push-the-freedombox-foundation-from-0-to-60-in-30\">Kickstarter\n"
"funders</a>.  And now that we know we can distribute <a\n"
"href=\"http://www.gnu.org/copyleft/gpl.html\">GPL</a>-compliant boxes we\n"
"can get those out too!\n"
msgstr ""

#. type: Plain text
msgid "That's the update.  More soon."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Best regards,\n"
"<a href=\"http://www.hackervisions.org\">James Vasile</a>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr ""
