# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# lkppo, 2012
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2014-11-25 21:35+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: French (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Announcing Freedombox-Privoxy\"]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Enhanced Privacy and Security for Web Browsing"
msgstr ""

#. type: Plain text
msgid ""
"One thing many people agree the FreedomBox should do is web filtering for "
"privacy and ad-removal.  Toward that end, the FreedomBox will act as a web "
"proxy to clean up and protect web traffic."
msgstr ""

#. type: Plain text
msgid ""
"We have a first draft version of privoxy [up on git](https://github.com/"
"jvasile/freedombox-privoxy).  It upgrades your web traffic to prefer ssl "
"encryption whereever it can.  It also strips tracking software from web "
"pages to give you greater privacy and anonymity as you surf."
msgstr ""

#. type: Plain text
msgid ""
"If you are a privoxy user, please do give this package a test run and report "
"any problems on the [issue tracker](https://github.com/jvasile/freedombox-"
"privoxy/issues).  We are working on upstreaming these changes to the privoxy "
"project, and in the mean time, you can make a debian package quite easily "
"from the git repository."
msgstr ""

#. type: Plain text
msgid ""
"Further work will include writing a script to test all the https-everywhere "
"rules and discard the ones that are broken.  As well as one to periodically "
"check for new regexes.  Anybody who wants to contribute to writing that is "
"welcome to jump on in!"
msgstr ""

#. type: Plain text
msgid ""
"More details about this part of FreedomBox can be found on [our code page]"
"(https://www.freedomboxfoundation.org/code/)."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!sidebar  content=\"\"\"\n"
msgstr "[[!sidebar  content=\"\"\"\n"

#. type: Plain text
#, no-wrap
msgid ""
"Links:\n"
"[[Home|https://www.freedomboxfoundation.org/]]  \n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[FAQ]]  \n"
msgstr ""

#. type: Plain text
msgid "[[Donate]]"
msgstr "[[Faire un don]]"
