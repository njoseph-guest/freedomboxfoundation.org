# Contact

If you are interested in joining the ongoing discussion about FreedomBox
with users and developers, sign up for our [Mailing
list](http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss).
For technical support, please refer to the same mailing list. If you
would like to contact the FreedomBox Foundation directly, our contact
information is below.

## Email

General information <info@freedomboxfoundation.org>

Press inquires <press@freedomboxfoundation.org>

If you are interested in supporting the FreedomBox project's efforts,
either as a developer or future user, please write to us and let us
know. <join@freedomboxfoundation.org>

If you are interested in making a financial contribution to the
Foundation, please write to <donate@freedomboxfoundation.org>

## Mailing Address

Freedom Box Foundation 1995 Broadway FL17 New York, NY 10023
