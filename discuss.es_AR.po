# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# fauno <fauno@kiwwwi.com.ar>, 2011
# fauno <fauno@kiwwwi.com.ar>, 2011
# Nicolás Reynolds <fauno@endefensadelsl.org>, 2011
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2015-01-08 17:47+0000\n"
"PO-Revision-Date: 2012-02-14 14:04+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Spanish (Argentina) (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/es_AR/)\n"
"Language: es_AR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Title #
#, no-wrap
msgid "Join the Discussion"
msgstr "Unirse a la discusión"

#. type: Plain text
msgid ""
"There are a variety of ways to keep up with project information and even "
"more ways to contribute."
msgstr ""
"Hay varias formas de mantenerse al tanto del proyecto y aún más formas de "
"contribuir."

#. type: Plain text
msgid ""
"Announcements will be made at [http://freedomboxfoundation.org](http://"
"freedomboxfoundation.org), and there is of course an [rss feed](http://www."
"freedomboxfoundation.org/index.rss)."
msgstr ""

#. type: Plain text
msgid ""
"You can discuss the project on the [Debian FreedomBox mailing list](http://"
"lists.alioth.debian.org/mailman/listinfo/freedombox-discuss), the [Debian "
"wiki](http://wiki.debian.org/FreedomBox) and in `#freedombox` on `irc.oftc."
"net`.  We spend time in the discussion venues and try to participate as time "
"allows."
msgstr ""
"Podés discutir el proyecto en la [lista de correo de FreedomBox en Debian]"
"(http://lists.alioth.debian.org/mailman/listinfo/freedombox-discuss) (en "
"inglés), el [wiki en Debian](http://wiki.debian.org/FreedomBox) y en "
"`#freedombox` en `irc.oftc.net`. Nos tomamos el tiempo de discutir y "
"tratamos de participar tanto como podamos."

#. type: Plain text
msgid ""
"If you want to contact the FreedomBox Foundation itself, see the [[contact]] "
"page."
msgstr ""
"Si te interesa contactarte con la Fundación FreedomBox, usa la página de "
"[[contacto|contact]]."

#~ msgid ""
#~ "The technical advisory committee has a mailing list. Its [archives]"
#~ "(http://lists.freedomboxfoundation.org/s/arc/tac) are viewable."
#~ msgstr ""
#~ "El Comité Asesor Técnico tiene una lista de correo. Sus [archivos](http://"
#~ "lists.freedomboxfoundation.org/s/arc/tac) son públicos."
