# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# clint <clint@debian.org>, 2011
# FULL NAME <EMAIL@ADDRESS>, 2011
msgid ""
msgstr ""
"Project-Id-Version: FreedomBox Foundation\n"
"POT-Creation-Date: 2016-02-24 17:43+0000\n"
"PO-Revision-Date: 2013-04-17 08:20+0000\n"
"Last-Translator: fbfwiki <root@softwarefreedom.org>\n"
"Language-Team: Korean (http://www.transifex.com/projects/p/"
"freedomboxfoundation/language/ko/)\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Title #
#, no-wrap
msgid "Help out without leaving your web browser"
msgstr "웹 브라우저를 닫지 마시고, 도움을 주시기 바랍니다. "

#. type: Plain text
msgid ""
"We generate a decent amount of video while telling people about the "
"FreedomBox project, almost all of which is in English. This is great for "
"people who speak English and like watching video but poses some difficulty "
"for the rest of the world. If you happen to enjoy our videos, you can help "
"other people access them by taking a minute to subtitle the video, turning "
"it instantly into text that can be widely accesses and translated."
msgstr ""
"저희는 사람들에게 프리덤 박스 프로젝트에 관해 설명을 하면서 상당히 많은 양의 "
"비디오들을 만들고 있습니다. 만들어지는 비디오들은 거의 모두 영어로 되어 있습"
"니다. 이것은 영어를 모국어로 하고 있고, 비디오 보는 것을 선호하는 사람들에게"
"는 좋은 일입니다. 그러나, 영어를 모국어로 하지 않는 사람들에게는 상당한 불편"
"을 초래합니다. 만약 저희의 비디오들을 보시고 만족스러우셨다면, 다른 사람들도 "
"비디오들을 보고 만족스러워 할 수 있는 기회를 주시기 바랍니다. 이것은 여러분께"
"서, 잠시 시간을 내주셔서, 비디오에 영어로 된 자막을 삽입함으로서 하실 수 있으"
"십니다. 그렇게 하시면, 후에 더 많은 사람들이 그 영어로 된 자막을 보고 각자 자"
"신이 사용하는 언어로 번역할 수 있게 됩니다. "

#. type: Plain text
msgid ""
"If you have a modern web browser installed, you already have everything you "
"need to help out. Simply visit our [media page](http://freedomboxfndn."
"mirocommunity.org) and look at the subtitle menu directly below each video. "
"If any of them say \"Subtitle Me!\" just click there and you are ready to "
"start. You will need an account with [Universal Subtitles](http://"
"universalsubtitles.org/), a project run by the [Participatory Culture "
"Foundation](http://pculture.org/)."
msgstr ""
"만약 근래에 나온 웹 브라우저가 당신의 컴퓨터에 설치되어 있다면, 당신은 이미 "
"다른 사람에게 도움을 주기 위한 준비가 완료된 상태입니다. 도움을 주시는 방법"
"은 간단합니다. 저희의 [미디어 페이지](http://freedomboxfndn.mirocommunity."
"org) 를 방문하셔서, 각 비디오 바로 밑에 있는 \"Subtitle Me (저에게 자막을 달"
"아주세요)\" 메뉴를 봐주시기 바랍니다. 그런 후, 그것을 클릭 해주시기 바랍니"
"다. 그렇게만 하시면, 자막을 만드실 준비가 끝난 것입니다. 이 작업을 수행하시"
"기 위해서는 [유니버셜 자막](http://universalsubtitles.org/)에 계좌를 가지고 "
"계셔야 합니다. \"유니버셜 자막\"은 [문화 참여 파운데이션](http://pculture."
"org/)에 의해 운영되는 프로젝트입니다."

#. type: Title ##
#, no-wrap
msgid "Current videos we need help subtitling"
msgstr "현재 저희가 자막을 만드는데 도움을 필요로 하는 비디오들"

#. type: Plain text
msgid ""
"Jame Vasile's \"[Elevate 2011](http://freedomboxfndn.mirocommunity.org/"
"video/9/elevate-2011)\" talk from the 2011 Elevate conference in Graz, "
"October 24, 2011"
msgstr ""

#. type: Plain text
msgid ""
"Bdale Garbee's \"[FreedomBox progress report](http://freedomboxfndn."
"mirocommunity.org/video/7/freedombox-progress-report)\" from the 2011 "
"DebConf, August 12, 2011"
msgstr ""
